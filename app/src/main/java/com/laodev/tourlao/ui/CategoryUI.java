package com.laodev.tourlao.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laodev.tourlao.R;
import com.laodev.tourlao.model.CategoryModel;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.squareup.picasso.Picasso;

public class CategoryUI extends LinearLayout {

    private Context mContext;

    private ImageView img_icon;
    private TextView lbl_name;

    private CategoryModel mModel = new CategoryModel();
    private CategoryUICallback callback;

    private void initEvent() {
        this.setOnClickListener(view -> callback.onClickCategoryItem(mModel));
    }

    public CategoryUI(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public CategoryUI(Context context){
        super(context);

        mContext = context;

        setOrientation(LinearLayout.VERTICAL);
        LayoutInflater.from(context).inflate(R.layout.ui_category, this, true);

        img_icon = findViewById(R.id.img_cate_icon);
        lbl_name = findViewById(R.id.lbl_cate_name);

        initEvent();
    }

    public CategoryUI(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    public void setModel(CategoryModel model) {
        mModel = model;

        Picasso.with(mContext).load(APIManager.UPLOAD_URL + "cat/" + mModel.imgurl).fit().centerCrop()
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(img_icon, null);
        lbl_name.setText(mModel.name);
    }

    public void setCategoryUICallback(CategoryUICallback callback) {
        this.callback = callback;
    }

    public interface CategoryUICallback {
        void onClickCategoryItem(CategoryModel mode);
    }

}
