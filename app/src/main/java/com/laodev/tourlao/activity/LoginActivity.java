package com.laodev.tourlao.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;
import com.laodev.tourlao.R;
import com.laodev.tourlao.model.UserModel;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private CallbackManager callbackManager;

    private TextInputEditText txt_email, txt_pass;
    private TextInputLayout tll_email, tll_pass;
    private CheckBox chk_remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        setTitle(getString(R.string.main_title));
        mAuth = FirebaseAuth.getInstance();

        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);

        initUIView();
    }

    private void initUIView() {
        tll_email = findViewById(R.id.tll_login_email);
        txt_email = findViewById(R.id.txt_login_email);
        txt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_email.setHelperText("");
            }
        });

        tll_pass = findViewById(R.id.tll_login_pass);
        txt_pass = findViewById(R.id.txt_login_pass);
        txt_pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_pass.setHelperText("");
            }
        });

        chk_remember = findViewById(R.id.chk_login_remember);

        SharedPreferences prefs = getSharedPreferences("com.laodev.tourlao", MODE_PRIVATE);
        String email = prefs.getString("email", "");
        String pass = prefs.getString("password", "");

        txt_email.setText(email);
        txt_pass.setText(pass);

        assert email != null;
        if (!email.equals("")) {
            chk_remember.setChecked(true);
        }

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        String userid = loginResult.getAccessToken().getUserId();
                        Profile profile = Profile.getCurrentProfile();

                        if (AppUtils.isFBMode) {
                            FireManager.getUserInfoByFBLogin(userid, profile, user -> {
                                AppUtils.isLoginFB = true;
                                AppUtils.gUser = user;
                                AppUtils.showOtherActivity(LoginActivity.this, HomeActivity.class, -1);
                            });
                        } else {

                        }
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        // alertdialog for exit the app
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.common_warning));
        alertDialogBuilder
                .setMessage(getString(R.string.login_exist_message))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.common_yes), (dialog, id) -> {
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                })

                .setNegativeButton(getString(R.string.common_no), (dialog, id) -> dialog.cancel());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onClickBtnLogin(View view) {
        String email = txt_email.getText().toString();
        if (email.length() == 0) {
            tll_email.setHelperText(getString(R.string.register_email_empty));
            return;
        }
        if (!(email.contains("@") || email.contains(".com"))) {
            tll_email.setHelperText(getString(R.string.register_email_wrong));
            return;
        }
        String pass = txt_pass.getText().toString();
        if (pass.length() < 6) {
            tll_pass.setHelperText(getString(R.string.register_pass_wrong));
            return;
        }

        mAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();

                        assert user != null;
                        if (user.isEmailVerified()) {
                            ProgressDialog dialog = AppUtils.onShowProgressDialog(this, getResources().getString(R.string.common_server_connect), false);

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference mRef = database.getReference().child("Users").child(user.getUid());
                            mRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                                    // This method is called once with the initial value and again
                                    // whenever data at this location is updated.
                                    AppUtils.onDismissProgressDialog(dialog);

                                    UserModel fUser = dataSnapshot.getValue(UserModel.class);

                                    Map<String, String> params = new HashMap<>();
                                    params.put("usr_uid", user.getUid());
                                    params.put("usr_email", email);
                                    params.put("usr_name", fUser.name);

                                    APIManager.onAPIConnectionResponse(APIManager.LAO_LOGIN, params, LoginActivity.this, APIManager.APIMethod.POST, (obj, ret) -> {
                                        try {
                                            if (ret == 10000) {
                                                SharedPreferences.Editor editor = getSharedPreferences("com.laodev.tourlao", MODE_PRIVATE).edit();
                                                if (chk_remember.isChecked()) {
                                                    editor.putString("email", email);
                                                    editor.putString("password", pass);
                                                    editor.apply();
                                                }

                                                JSONObject result = obj.getJSONObject("result");

                                                AppUtils.gUser.initWithJSON(result);
                                                AppUtils.showOtherActivity(LoginActivity.this, HomeActivity.class, -1);
                                            } else {
                                                String msg = obj.getString("msg");
                                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }
                                    });
                                }

                                @Override
                                public void onCancelled(@NotNull DatabaseError error) {
                                    // Failed to read value
                                    AppUtils.onDismissProgressDialog(dialog);
                                }
                            });
                        } else {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.common_failed), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void onClickBtnSignUp(View view) {
        AppUtils.showOtherActivity(this, RegisterActivity.class, 0);
    }

    public void onClickLblForgot(View view) {
        AppUtils.showOtherActivity(this, ForgotPassActivity.class, 0);
    }

    public void onClickLltFBLogin(View view) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if (isLoggedIn) {
            String userid = accessToken.getUserId();
            Profile profile = Profile.getCurrentProfile();

            ProgressDialog dialog = AppUtils.onShowProgressDialog(this, getResources().getString(R.string.common_server_connect), false);
            FireManager.getUserInfoByFBLogin(userid, profile, user -> {
                AppUtils.isLoginFB = true;
                AppUtils.gUser = user;
                AppUtils.showOtherActivity(LoginActivity.this, HomeActivity.class, -1);
                dialog.dismiss();
            });
        } else {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        }
    }
}
