package com.laodev.tourlao.activity;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.laodev.tourlao.R;
import com.laodev.tourlao.model.UserModel;
import com.laodev.tourlao.utils.AppUtils;

public class RegisterActivity extends AppCompatActivity {

    private UserModel user = new UserModel();
    private FirebaseAuth mAuth;

    private TextInputEditText txt_email, txt_pass, txt_name, txt_cpass;
    private TextInputLayout tll_email, tll_pass, tll_name, tll_cpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        mAuth = FirebaseAuth.getInstance();

        setTitle(getString(R.string.main_title));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);

        initUIView();
    }

    private void initUIView() {
        tll_name = findViewById(R.id.tll_register_name);
        txt_name = findViewById(R.id.txt_register_name);
        txt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_name.setHelperText("");
            }
        });

        tll_email = findViewById(R.id.tll_register_email);
        txt_email = findViewById(R.id.txt_register_email);
        txt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_email.setHelperText("");
            }
        });

        tll_pass = findViewById(R.id.tll_register_pass);
        txt_pass = findViewById(R.id.txt_register_pass);
        txt_pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_pass.setHelperText("");
            }
        });

        tll_cpass = findViewById(R.id.tll_register_cpass);
        txt_cpass = findViewById(R.id.txt_register_cpass);
        txt_cpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_cpass.setHelperText("");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickBtnRegister(View view) {
        String name = txt_name.getText().toString();
        if (name.length() == 0) {
            tll_name.setHelperText(getString(R.string.register_name_wrong));
            return;
        }
        String email = txt_email.getText().toString();
        if (email.length() == 0) {
            tll_email.setHelperText(getString(R.string.register_email_empty));
            return;
        }
        if (!(email.contains("@") || email.contains(".com"))) {
            tll_email.setHelperText(getString(R.string.register_email_wrong));
            return;
        }
        String pass = txt_pass.getText().toString();
        if (pass.length() < 6) {
            tll_pass.setHelperText(getString(R.string.register_pass_wrong));
            return;
        }
        String cpass = txt_cpass.getText().toString();
        if (!pass.equals(cpass)) {
            tll_cpass.setHelperText(getString(R.string.register_cpass_wrong));
            return;
        }

        user.name = name;
        user.email = email;
        createAccount(pass);
    }

    private void createAccount(String password) {
        ProgressDialog dialog = AppUtils.onShowProgressDialog(this, getResources().getString(R.string.common_server_connect), false);
        mAuth.createUserWithEmailAndPassword(user.email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Toast.makeText(RegisterActivity.this, "Success", Toast.LENGTH_SHORT).show();

                        FirebaseUser currentUser = mAuth.getCurrentUser();
                        user.id = currentUser.getUid();

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference mRef = database.getReference().child("Users").child(user.id);

                        mRef.setValue(user);

                        currentUser.sendEmailVerification()
                                .addOnCompleteListener(task1 -> {
                                    AppUtils.onDismissProgressDialog(dialog);
                                    if (task1.isSuccessful()) {
                                        // after email is sent just logout the user and finish this activity
                                        FirebaseAuth.getInstance().signOut();
                                        AppUtils.showOtherActivity(this, LoginActivity.class, 1);
                                        finish();
                                    } else {
                                        overridePendingTransition(0, 0);
                                        finish();
                                    }
                                });
                    } else {
                        // If sign in fails, display a message to the user.
                        AppUtils.onDismissProgressDialog(dialog);
                        Toast.makeText(RegisterActivity.this, getString(R.string.common_failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
