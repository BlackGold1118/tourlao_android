package com.laodev.tourlao.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.laodev.tourlao.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
