package com.laodev.tourlao.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.laodev.tourlao.BuildConfig;
import com.laodev.tourlao.R;
import com.laodev.tourlao.adapter.BannerAdapter;
import com.laodev.tourlao.adapter.ProductAdapter;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.model.ProductModel;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CategoryActivity extends AppCompatActivity {

    private ProductAdapter adapter;

    private List<ProductModel> models = new ArrayList<>();
    private List<ProductModel> showModels = new ArrayList<>();

    private List<CommentModel> mComments = new ArrayList<>();

    private LinearLayout llt_search;
    private EditText txt_search;
    private ListView lst_banner;

    private int banner_index = 0;
    private int change_index = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        setTitle(AppUtils.gCategoryModel.name);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);

        initUIView();
    }

    private void initWithdDatas() {
        llt_search.setVisibility(View.GONE);
        txt_search.setText("");

        Map<String, String> params = new HashMap<>();
        params.put("cat_id", AppUtils.gCategoryModel.id);
        params.put("usr_id", AppUtils.gUser.id);

        APIManager.onAPIConnectionResponse(APIManager.LAO_CATEGORY_SPOT, params, this, APIManager.APIMethod.POST, (obj, ret) -> {
            try {
                if (ret == 10000) {
                    models.clear();
                    showModels.clear();

                    JSONArray result = obj.getJSONArray("result");
                    for (int i = 0; i <  result.length(); i++) {
                        JSONObject pro_obj = result.getJSONObject(i);

                        ProductModel product = new ProductModel();
                        product.initWithJSON(pro_obj);

                        models.add(product);
                    }
                    showModels.addAll(models);
                    adapter.notifyDataSetChanged();
                } else {
                    String msg = obj.getString("msg");
                    Toast.makeText(CategoryActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(CategoryActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }

    private void initUIView() {
        llt_search = findViewById(R.id.llt_category_search);
        txt_search = findViewById(R.id.txt_category_search);
        txt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                showModels.clear();
                for (ProductModel model: models) {
                    if (model.isContainSearchKey(CategoryActivity.this, s.toString())) {
                        showModels.add(model);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });

//        LinearLayout llt_setting = findViewById(R.id.llt_category_setting);
//        if (category.id.equals("6")) {
//            llt_setting.setVisibility(View.VISIBLE);
//        }

        adapter = new ProductAdapter(this, showModels, mComments);
        adapter.setProductCellCallback(new ProductAdapter.ProductCellCallback() {
            @Override
            public void onClickProductCallback(ProductModel model) {
                AppUtils.gProduct = model;
                AppUtils.showOtherActivity(CategoryActivity.this, ProductDetailActivity.class, 0);
            }

            @Override
            public void onClickLikeImageCallback(ProductModel model, List<String> users) {
                if (AppUtils.isFBMode) {
                    List<String> changeUsers = new ArrayList<>();
                    changeUsers.addAll(users);
                    if (changeUsers.contains(AppUtils.gUser.id)) {
                        changeUsers.remove(AppUtils.gUser.id);
                        Toast.makeText(CategoryActivity.this, getString(R.string.category_remove_like), Toast.LENGTH_SHORT).show();
                    } else {
                        changeUsers.add(AppUtils.gUser.id);
                        Toast.makeText(CategoryActivity.this, getString(R.string.category_add_like), Toast.LENGTH_SHORT).show();
                    }
                    FireManager.changeLikeFromFirebase(model.id, changeUsers, () -> adapter.notifyDataSetChanged());
                } else {
                    Map<String, String> params = new HashMap<>();
                    params.put("spot_id", model.id);
                    params.put("usr_id", AppUtils.gUser.id);

                    APIManager.onAPIConnectionResponse(APIManager.LAO_SET_LIKE, params, CategoryActivity.this, APIManager.APIMethod.POST, (obj, ret) -> {
                        try {
                            if (ret == 10000) {
                                initWithdDatas();
                            } else {
                                String msg = obj.getString("msg");
                                Toast.makeText(CategoryActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(CategoryActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
                }
            }

            @Override
            public void onClickLikeLabelCallback(ProductModel model) {
                if (!AppUtils.isFBMode) {
                    Map<String, String> params = new HashMap<>();
                    params.put("spot_id", model.id);
                    params.put("usr_id", AppUtils.gUser.id);

                    APIManager.onAPIConnectionResponse(APIManager.LAO_SET_LIKE, params
                            , CategoryActivity.this
                            , APIManager.APIMethod.POST, (obj, ret) -> {
                        try {
                            if (ret == 10000) {
                                initWithdDatas();
                            } else {
                                String msg = obj.getString("msg");
                                Toast.makeText(CategoryActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(CategoryActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
                }
            }

            @Override
            public void onClickCommentCallback(ProductModel model) {
                AppUtils.gProduct = model;
                AppUtils.showOtherActivity(CategoryActivity.this, CommentActivtiy.class, 0);
            }

            @Override
            public void onClickShareCallback(ProductModel model) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    String shareMessage= "";
                    String localization = Locale.getDefault().getLanguage();
                    if (localization.equals("lo")) {
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, model.name_la);
                        shareMessage = "\n" + model.detail_la + "\n\n";
                    } else {
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, model.name);
                        shareMessage = "\n" + model.detail + "\n\n";
                    }

                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "Choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
            }

        });

        ListView lst_category = findViewById(R.id.lst_category);
        lst_category.setAdapter(adapter);

        BannerAdapter bannerAdapter = new BannerAdapter(this);
        lst_banner = findViewById(R.id.lst_category_banner);
        lst_banner.setAdapter(bannerAdapter);
        lst_banner.setOnItemClickListener((parent, view, position, id) -> {
            onShowBannerDetail(position);
        });
        lst_banner.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                return true; // Indicates that this has been handled by you and will not be forwarded further.
            }
            return false;
        });
        onSliderBanner();
    }

    private void onShowBannerDetail(int position) {
        String url = "";
        switch (position) {
            case 0:
                url = "http://www.etllao.com/";
                break;
            case 1:
                url = "http://laoplazahotel.com/";
                break;
            case 3:
                url = "https://laotel.com/";
                break;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        this.startActivity(browserIntent);
    }

    private void onSliderBanner() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            //Do something after 1500ms
            banner_index = banner_index + change_index;
            if (banner_index == 2) {
                change_index = -1;
            }
            if (banner_index == 0) {
                change_index = 1;
            }
            lst_banner.smoothScrollToPosition(banner_index);
            onSliderBanner();
        }, 2000);
    }

    private void onSortByName() {
        Collections.sort(showModels, (Comparator) (o1, o2) -> {
            ProductModel p1 = (ProductModel) o1;
            ProductModel p2 = (ProductModel) o2;
            String localization = Locale.getDefault().getLanguage();
            if (localization.equals("lo")) {
                return p1.name_la.compareToIgnoreCase(p2.name_la);
            } else {
                return p1.name.compareToIgnoreCase(p2.name);
            }
        });
        adapter.notifyDataSetChanged();
    }

    private void onSortByPrice() {
        Collections.sort(showModels, (Comparator) (o1, o2) -> {
            ProductModel p1 = (ProductModel) o1;
            ProductModel p2 = (ProductModel) o2;
            return p1.price.compareToIgnoreCase(p2.price);
        });
        adapter.notifyDataSetChanged();
    }

    private void onSortByLocation() {
        Collections.sort(showModels, (Comparator) (o1, o2) -> {
            ProductModel p1 = (ProductModel) o1;
            ProductModel p2 = (ProductModel) o2;
            return p1.province.compareToIgnoreCase(p2.province);
        });
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return false;
            case R.id.menu_category_search:
                if (llt_search.getVisibility() == View.GONE) {
                    llt_search.setVisibility(View.VISIBLE);
                } else {
                    llt_search.setVisibility(View.GONE);
                    AppUtils.hideKeyboardFrom(this, txt_search);
                }
                return false;
            case  R.id.menu_category_name:
                onSortByName();
                return false;
            case R.id.menu_category_time:
                onSortByPrice();
                return false;
            case R.id.menu_category_location:
                onSortByLocation();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickADImg(View view) {
        String url = "";
        switch (view.getTag().toString()) {
            case "home_etl":
                url = "http://www.etllao.com/";
                break;
            case "home_plaza":
                url = "http://laoplazahotel.com/";
                break;
            case "home_telecome":
                url = "https://laotel.com/";
                break;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initWithdDatas();
    }

}
