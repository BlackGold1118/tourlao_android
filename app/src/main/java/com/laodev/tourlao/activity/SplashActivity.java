package com.laodev.tourlao.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.laodev.tourlao.R;
import com.laodev.tourlao.utils.AppUtils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Change Status Bar Color
        AppUtils.initUIActivity(this);

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            //Do something after 1500ms
            onShowMainActivity();
        }, 1500);
    }

    private void onShowMainActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }


}
