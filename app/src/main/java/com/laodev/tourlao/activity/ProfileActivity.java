package com.laodev.tourlao.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.laodev.tourlao.R;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.model.UserModel;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.listeners.OnCountryPickerListener;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    private ImageView img_avatar;
    private TextInputLayout tll_name, tll_email, tll_country, tll_opass, tll_npass, tll_cpass;
    private TextInputEditText txt_name, txt_email, txt_country, txt_opass, txt_npass, txt_cpass;

    private CountryPicker.Builder builder =
            new CountryPicker.Builder().with(this)
                    .listener(new OnCountryPickerListener() {
                        @Override
                        public void onSelectCountry(Country country) {
                            txt_country.setText(country.getName());
                            isShowDialog = false;
                        }
                    })
                    .style(R.style.CountryPickerStyle);
    private CountryPicker picker = builder.build();

    private boolean isShowDialog = false;
    private UserModel user = AppUtils.gUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        setTitle(getString(R.string.common_profile));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);

        initUIView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUIView() {
        img_avatar = findViewById(R.id.img_profile_avatar);
        if (user.photo.length() > 0) {
            Picasso.with(this).load(user.photo).fit().centerCrop()
                    .placeholder(R.drawable.ic_user_avatar)
                    .error(R.drawable.ic_user_avatar)
                    .into(img_avatar, null);
        }

        tll_name = findViewById(R.id.tll_profile_name);
        txt_name = findViewById(R.id.txt_profile_name);
        txt_name.setText(user.name);
        txt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_name.setHelperText("");
            }
        });

        tll_email= findViewById(R.id.tll_profile_email);
        txt_email = findViewById(R.id.txt_profile_email);
        txt_email.setText(user.email);
        tll_email.setEnabled(false);

        tll_country = findViewById(R.id.tll_profile_country);
        txt_country = findViewById(R.id.txt_profile_country);
        txt_country.setText(user.country);
        txt_country.setOnTouchListener((view, motionEvent) -> {
            if (!isShowDialog) {
                picker.showDialog(ProfileActivity.this);
                isShowDialog = true;
            }
            return true;
        });

        tll_opass = findViewById(R.id.tll_profile_opass);
        txt_opass = findViewById(R.id.txt_profile_opass);
        txt_opass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_opass.setHelperText("");
            }
        });

        tll_npass = findViewById(R.id.tll_profile_npass);
        txt_npass = findViewById(R.id.txt_profile_npass);
        txt_npass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_npass.setHelperText("");
            }
        });

        tll_cpass = findViewById(R.id.tll_profile_cpass);
        txt_cpass = findViewById(R.id.txt_profile_cpass);
        txt_cpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_cpass.setHelperText("");
            }
        });

        if (AppUtils.isLoginFB) {
            tll_opass.setEnabled(false);
            tll_npass.setEnabled(false);
            tll_cpass.setEnabled(false);
        }

        FloatingActionButton fab_spot = findViewById(R.id.fab_profile_spot);
        fab_spot.setOnClickListener(view -> AppUtils.showOtherActivity(ProfileActivity.this, AddProductActivity.class, 0));
        Drawable myFabSrc = getDrawable(R.drawable.ic_add_white);
        Drawable willBeWhite = myFabSrc.getConstantState().newDrawable();
        willBeWhite.mutate().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        fab_spot.setImageDrawable(willBeWhite);
        if (user.type == 0) {
            fab_spot.setVisibility(View.GONE);
        }
    }

    public void onClickAddPhoto(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 10001);
            return;
        }
        CropImage.activity()
                .setMaxCropResultSize(AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT)
                .setAutoZoomEnabled(false)
                .start(this);
    }

    public void onClickUpdateBtn(View view) {
        if (user.photo.equals("")) {
            Toast.makeText(this, getString(R.string.profile_error_photo), Toast.LENGTH_SHORT).show();
            return;
        }

        String name = txt_name.getText().toString();
        if (name.length() == 0) {
            tll_name.setHelperText(getString(R.string.register_name_wrong));
            return;
        }

        String country = txt_country.getText().toString();
        if (country.length() == 0) {
            tll_country.setHelperText(getString(R.string.profile_error_country));
            return;
        }

        String opass = txt_opass.getText().toString();
        if (opass.length() > 0) {
            String npass = txt_npass.getText().toString();
            if (npass.length() < 6) {
                tll_npass.setHelperText(getString(R.string.register_pass_wrong));
                return;
            }
            String cpass = txt_cpass.getText().toString();
            if (!npass.equals(cpass)) {
                tll_cpass.setHelperText(getString(R.string.register_cpass_wrong));
                return;
            }

                ProgressDialog dialog = AppUtils.onShowProgressDialog(this, getResources().getString(R.string.common_server_connect), false);
                FireManager.changePassword(AppUtils.gUser.email, opass, npass, new FireManager.ChangePasswordCallback() {
                    @Override
                    public void onCallbackSuccess() {
                        dialog.dismiss();

                        user.name = name;
                        user.country = country;

                        if (AppUtils.isFBMode) {
                            FireManager.updateUserFromFirebase(user, user -> {
                                AppUtils.gUser = user;
                                onBackPressed();
                            });
                        } else {
                            Map<String, String> params = new HashMap<>();
                            params.put("usr_id", AppUtils.gUser.id);
                            params.put("usr_opass", opass);
                            params.put("usr_npass", cpass);

                            APIManager.onAPIConnectionResponse(APIManager.LAO_CH_PASS
                                    , params, ProfileActivity.this
                                    , APIManager.APIMethod.POST, (obj, ret) -> {
                                        try {
                                            if (ret == 10000) {
                                                updateUser(name, country);
                                            } else {
                                                String msg = obj.getString("msg");
                                                Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
                                            }
                                            onBackPressed();
                                        } catch (JSONException e) {
                                            Toast.makeText(ProfileActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCallbackIncorrectPass() {
                        dialog.dismiss();
                        Toast.makeText(ProfileActivity.this, getString(R.string.profile_error_pass), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCallbackFailed() {
                        dialog.dismiss();
                        Toast.makeText(ProfileActivity.this, getString(R.string.commen_error_server), Toast.LENGTH_SHORT).show();
                    }
                });
        } else {
            user.name = name;
            user.country = country;
            if (AppUtils.isFBMode) {
                ProgressDialog dialog = AppUtils.onShowProgressDialog(this, getResources().getString(R.string.common_server_connect), false);
                FireManager.updateUserFromFirebase(user, user -> {
                    dialog.dismiss();

                    AppUtils.gUser = user;
                    onBackPressed();
                });
            } else {
                updateUser(name, country);
            }
        }
    }

    private void updateUser(String name, String country) {
        Map<String, String> params = new HashMap<>();
        params.put("usr_id", AppUtils.gUser.id);
        params.put("usr_name", name);
        params.put("usr_country", country);

        APIManager.onAPIConnectionResponse(APIManager.LAO_UP_USER
                , params, ProfileActivity.this
                , APIManager.APIMethod.POST, (obj, ret) -> {
                    try {
                        if (ret == 10000) {
                            JSONObject result = obj.getJSONObject("result");
                            AppUtils.gUser.initWithJSON(result);
                        } else {
                            String msg = obj.getString("msg");
                            Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                        onBackPressed();
                    } catch (JSONException e) {
                        Toast.makeText(ProfileActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (AppUtils.isFBMode) {
                    FireManager.setUserAvatarToFirebase(resultUri, url -> {
                        user.photo = url;
                        Picasso.with(ProfileActivity.this).load(user.photo).fit().centerCrop()
                                .placeholder(R.drawable.ic_user_avatar)
                                .error(R.drawable.ic_user_avatar)
                                .into(img_avatar, null);
                    });
                } else {
                    String encodedImage = AppUtils.encodeImage(resultUri.getPath());

                    Map<String, String> params = new HashMap<>();
                    params.put("usr_id", AppUtils.gUser.id);
                    params.put("base64", encodedImage);

                    APIManager.onAPIConnectionResponse(APIManager.LAO_SET_AVATAR
                            , params, ProfileActivity.this
                            , APIManager.APIMethod.POST, (obj, ret) -> {
                                try {
                                    if (ret == 10000) {
                                        JSONObject object = obj.getJSONObject("result");
                                        AppUtils.gUser.initWithJSON(object);
                                    } else {
                                        String msg = obj.getString("msg");
                                        Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
                                    }
                                    onBackPressed();
                                } catch (JSONException e) {
                                    Toast.makeText(ProfileActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            });
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 10001) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.activity()
                        .setMaxCropResultSize(AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT)
                        .setAutoZoomEnabled(false)
                        .start(this);
            } else {
                Toast.makeText(this, getString(R.string.common_permission_denied), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
