package com.laodev.tourlao.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.laodev.tourlao.BuildConfig;
import com.laodev.tourlao.R;
import com.laodev.tourlao.adapter.ProductSliderAdapter;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.model.ProductModel;
import com.laodev.tourlao.model.ProvinceModel;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.taufiqrahman.reviewratings.BarLabels;
import com.taufiqrahman.reviewratings.RatingReviews;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ProductDetailActivity extends AppCompatActivity {

    private String localization = Locale.getDefault().getLanguage();

    private List<CommentModel> mComments = new ArrayList<>();
    private List<ImageView> star1 = new ArrayList<>();
    private List<ImageView> star2 = new ArrayList<>();

    private TextView lbl_no_review, lbl_more, lbl_rating;
    private RatingReviews ratingReviews;
    private LinearLayout llt_comment1, llt_comment2;
    private ImageView img_avatar1, img_star11, img_star12, img_star13, img_star14, img_star15;
    private TextView lbl_name1, lbl_country1, lbl_comment1;
    private ImageView img_avatar2, img_star21, img_star22, img_star23, img_star24, img_star25;
    private TextView lbl_name2, lbl_country2, lbl_comment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }


        if (localization.equals("lo")) {
            setTitle(AppUtils.gProduct.name_la);
        } else {
            setTitle(AppUtils.gProduct.name);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);

        initUIView();
        initWithdDatas();
    }

    private void initWithdDatas() {
        if (AppUtils.isFBMode) {
            FireManager.getCommentFromFirebase(new FireManager.GetCommentCallback() {
                @Override
                public void onCallbackSuccess(List<CommentModel> comments) {
                    mComments.clear();
                    for (CommentModel comment: comments) {
                        if (comment.postid.equals(AppUtils.gProduct.id)) {
                            mComments.add(comment);
                        }
                    }
                    initWithComments();
                }

                @Override
                public void onCallbackFailed(String error) {
                    //
                }
            });
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("spot_id", AppUtils.gProduct.id);
            APIManager.onAPIConnectionResponse(APIManager.LAO_GET_COMMNET
                    , params, ProductDetailActivity.this
                    , APIManager.APIMethod.POST, (obj, ret) -> {
                try {
                    if (ret == 10000) {
                        mComments.clear();

                        JSONArray objArray = obj.getJSONArray("result");
                        for (int i = 0; i < objArray.length(); i++) {
                            JSONObject obj_model = objArray.getJSONObject(i);
                            CommentModel model = new CommentModel();
                            model.initWithJSON(obj_model);
                            mComments.add(model);
                        }
                        initWithComments();
                    } else {
                        String msg = obj.getString("msg");
                        Toast.makeText(ProductDetailActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(ProductDetailActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            });
        }
    }

    private void initUIView() {
        SliderView sliderView = findViewById(R.id.sld_product_detail);

        ProductSliderAdapter adapter = new ProductSliderAdapter(this, AppUtils.gProduct.photo);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.startAutoCycle();

        TextView lbl_desc = findViewById(R.id.lbl_detail_desc);
        if (localization.equals("lo")) {
            lbl_desc.setText(AppUtils.gProduct.detail_la);
        } else {
            lbl_desc.setText(AppUtils.gProduct.detail);
        }

        ProvinceModel provinceModel = AppUtils.getProvinceByID(AppUtils.gProduct.province);

        TextView lbl_province = findViewById(R.id.lbl_detail_province);
        String province = provinceModel.name;
        SpannableString content = new SpannableString(province);
        content.setSpan(new UnderlineSpan(), 0, province.length(), 0);
        lbl_province.setText(content);

        TextView lbl_province_desc = findViewById(R.id.lbl_detail_province_desc);
        String province_desc = provinceModel.desc;
        lbl_province_desc.setText(province_desc);

        TextView lbl_phone1 = findViewById(R.id.lbl_detail_phone1);
        SpannableString span_phone1 = new SpannableString(AppUtils.gProduct.phone1);
        span_phone1.setSpan(new UnderlineSpan(), 0, AppUtils.gProduct.phone1.length(), 0);
        lbl_phone1.setText(span_phone1);

        TextView lbl_phone2 = findViewById(R.id.lbl_detail_phone2);
        SpannableString span_phone2 = new SpannableString(AppUtils.gProduct.phone2);
        span_phone2.setSpan(new UnderlineSpan(), 0, AppUtils.gProduct.phone2.length(), 0);
        lbl_phone2.setText(span_phone2);

        TextView lbl_email = findViewById(R.id.lbl_detail_email);
        lbl_email.setText(AppUtils.gProduct.email);

        TextView lbl_price = findViewById(R.id.lbl_detail_price);
        lbl_price.setText(AppUtils.gProduct.price);

        lbl_no_review = findViewById(R.id.lbl_detail_noreview);

        ratingReviews = findViewById(R.id.rating_reviews);
        lbl_rating = findViewById(R.id.lbl_product_rating);

        llt_comment1 = findViewById(R.id.llt_detail_comment1);
        img_avatar1 = findViewById(R.id.img_detail_avatar1);
        img_star11 = findViewById(R.id.img_detail_star11);
        img_star12 = findViewById(R.id.img_detail_star12);
        img_star13 = findViewById(R.id.img_detail_star13);
        img_star14 = findViewById(R.id.img_detail_star14);
        img_star15 = findViewById(R.id.img_detail_star15);
        star1.add(img_star11);
        star1.add(img_star12);
        star1.add(img_star13);
        star1.add(img_star14);
        star1.add(img_star15);
        lbl_name1 = findViewById(R.id.lbl_detail_name1);
        lbl_country1 = findViewById(R.id.lbl_detail_country1);
        lbl_comment1 = findViewById(R.id.lbl_detail_comment1);

        llt_comment2 = findViewById(R.id.llt_detail_comment2);
        img_avatar2 = findViewById(R.id.img_detail_avatar2);
        img_star21 = findViewById(R.id.img_detail_star21);
        img_star22 = findViewById(R.id.img_detail_star22);
        img_star23 = findViewById(R.id.img_detail_star23);
        img_star24 = findViewById(R.id.img_detail_star24);
        img_star25 = findViewById(R.id.img_detail_star25);
        star2.add(img_star21);
        star2.add(img_star22);
        star2.add(img_star23);
        star2.add(img_star24);
        star2.add(img_star25);
        lbl_name2 = findViewById(R.id.lbl_detail_name2);
        lbl_country2 = findViewById(R.id.lbl_detail_country2);
        lbl_comment2 = findViewById(R.id.lbl_detail_comment2);

        lbl_more = findViewById(R.id.lbl_detail_more);
    }

    private void initWithComments() {
        int all_rating = 0;
        switch (mComments.size()) {
            case 0:
                lbl_no_review.setVisibility(View.VISIBLE);
                ratingReviews.setVisibility(View.GONE);
                llt_comment1.setVisibility(View.GONE);
                llt_comment2.setVisibility(View.GONE);
                lbl_more.setVisibility(View.GONE);
                break;
            case 1:
                initWithComment1();
                lbl_no_review.setVisibility(View.GONE);
                ratingReviews.setVisibility(View.VISIBLE);
                llt_comment1.setVisibility(View.VISIBLE);
                llt_comment2.setVisibility(View.GONE);
                lbl_more.setVisibility(View.GONE);
                break;
            case 2:
                initWithComment1();
                initWithComment2();
                lbl_no_review.setVisibility(View.GONE);
                ratingReviews.setVisibility(View.VISIBLE);
                llt_comment1.setVisibility(View.VISIBLE);
                llt_comment2.setVisibility(View.VISIBLE);
                lbl_more.setVisibility(View.GONE);
                break;
            default:
                initWithComment1();
                initWithComment2();
                lbl_no_review.setVisibility(View.GONE);
                ratingReviews.setVisibility(View.VISIBLE);
                llt_comment1.setVisibility(View.VISIBLE);
                llt_comment2.setVisibility(View.VISIBLE);
                lbl_more.setVisibility(View.VISIBLE);
                break;
        }
        if (mComments.size() > 0) {
            int r1 = 0, r2 = 0, r3 = 0, r4 = 0, r5 = 0;
            for (CommentModel comment: mComments) {
                switch (comment.rate) {
                    case 1:
                        r1++;
                        break;
                    case 2:
                        r2++;
                        break;
                    case 3:
                        r3++;
                        break;
                    case 4:
                        r4++;
                        break;
                    case 5:
                        r5++;
                        break;
                }
                all_rating = all_rating + comment.rate;
            }
            int[] colors = new int[]{
                    Color.parseColor("#0e9d58"),
                    Color.parseColor("#bfd047"),
                    Color.parseColor("#ffc105"),
                    Color.parseColor("#ef7e14"),
                    Color.parseColor("#d36259")};
            int[] raters = new int[]{
                    r5, r4, r3, r2, r1
            };
            ratingReviews.createRatingBars(100, BarLabels.STYPE1, colors, raters);
        }
        if (mComments.size() > 0) {
            float val_rate = (Float.valueOf(all_rating) / mComments.size());
            String str_rate = "";
            if (mComments.size() > 1000000) {
                str_rate  = String.format("%.1f / %.1f M", val_rate, Float.valueOf(mComments.size()) / 1000000);
            } else if (mComments.size() > 1000) {
                str_rate  = String.format("%.1f / %.1f K", val_rate, Float.valueOf(mComments.size()) / 1000);
            } else {
                str_rate  = String.format("%.1f / %d", val_rate, mComments.size());
            }
            lbl_rating.setText(str_rate);
        } else {
            lbl_rating.setText("");
        }
    }

    private void initWithComment1() {
        CommentModel comment = mComments.get(0);
        for (int i = comment.rate; i < 5; i++) {
            star1.get(i).setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
        }
        lbl_comment1.setText(comment.content);
        if (AppUtils.isFBMode) {
            FireManager.getUserFromFirebase(comment.userid, user -> {
                Picasso.with(ProductDetailActivity.this).load(user.photo).fit().centerCrop()
                        .placeholder(R.drawable.ic_user_avatar)
                        .error(R.drawable.ic_user_avatar)
                        .into(img_avatar1, null);
                lbl_name1.setText(user.name);
                lbl_country1.setText(user.country);
            });
        } else {
            Picasso.with(ProductDetailActivity.this).load(comment.usr_img).fit().centerCrop()
                    .placeholder(R.drawable.ic_user_avatar)
                    .error(R.drawable.ic_user_avatar)
                    .into(img_avatar1, null);
            lbl_name1.setText(comment.usr_name);
            lbl_country1.setText(comment.usr_country);
        }
    }

    private void initWithComment2() {
        CommentModel comment = mComments.get(1);
        for (int i = comment.rate; i < 5; i++) {
            star2.get(i).setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
        }
        lbl_comment2.setText(comment.content);
        if (AppUtils.isFBMode) {
            FireManager.getUserFromFirebase(comment.userid, user -> {
                Picasso.with(ProductDetailActivity.this).load(user.photo).fit().centerCrop()
                        .placeholder(R.drawable.ic_user_avatar)
                        .error(R.drawable.ic_user_avatar)
                        .into(img_avatar2, null);
                lbl_name2.setText(user.name);
                lbl_country2.setText(user.country);
            });
        } else {
            Picasso.with(ProductDetailActivity.this).load(comment.usr_img).fit().centerCrop()
                    .placeholder(R.drawable.ic_user_avatar)
                    .error(R.drawable.ic_user_avatar)
                    .into(img_avatar2, null);
            lbl_name2.setText(comment.usr_name);
            lbl_country2.setText(comment.usr_country);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return false;
            case R.id.menu_detail_comment:
                addComment();
                return false;
            case R.id.menu_detail_share:
                try {
                    ProductModel model = AppUtils.gProduct;
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    String shareMessage= "";
                    String localization = Locale.getDefault().getLanguage();
                    if (localization.equals("lo")) {
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, model.name_la);
                        shareMessage = "\n" + model.detail_la + "\n\n";
                    } else {
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, model.name);
                        shareMessage = "\n" + model.detail + "\n\n";
                    }

                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "Choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addComment() {
        CommentModel model = new CommentModel();
        model.postid = AppUtils.gProduct.id;
        model.userid = AppUtils.gUser.id;
        model.commentid = "";
        model.rate = 0;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy - MM - dd", Locale.getDefault());
        String currentDate = sdf.format(new Date());
        model.date = currentDate;

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.dialog_comment);
        dialog.setTitle(null);
        dialog.setCanceledOnTouchOutside(true);

        ImageView img_star1 = dialog.findViewById(R.id.img_dialog_star1);
        ImageView img_star2 = dialog.findViewById(R.id.img_dialog_star2);
        ImageView img_star3 = dialog.findViewById(R.id.img_dialog_star3);
        ImageView img_star4 = dialog.findViewById(R.id.img_dialog_star4);
        ImageView img_star5 = dialog.findViewById(R.id.img_dialog_star5);

        List<ImageView> img_stars = new ArrayList<>();
        img_stars.add(img_star1);
        img_stars.add(img_star2);
        img_stars.add(img_star3);
        img_stars.add(img_star4);
        img_stars.add(img_star5);

        img_star1.setOnClickListener(view -> {
            model.rate = 1;
            for (int i = 0; i < 5; i++) {
                ImageView img_star = img_stars.get(i);
                if (i < model.rate) {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_blue));
                } else {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
                }
            }
        });

        img_star2.setOnClickListener(view -> {
            model.rate = 2;
            for (int i = 0; i < 5; i++) {
                ImageView img_star = img_stars.get(i);
                if (i < model.rate) {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_blue));
                } else {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
                }
            }
        });

        img_star3.setOnClickListener(view -> {
            model.rate = 3;
            for (int i = 0; i < 5; i++) {
                ImageView img_star = img_stars.get(i);
                if (i < model.rate) {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_blue));
                } else {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
                }
            }
        });

        img_star4.setOnClickListener(view -> {
            model.rate = 4;
            for (int i = 0; i < 5; i++) {
                ImageView img_star = img_stars.get(i);
                if (i < model.rate) {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_blue));
                } else {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
                }
            }
        });

        img_star5.setOnClickListener(view -> {
            model.rate = 5;
            for (int i = 0; i < 5; i++) {
                ImageView img_star = img_stars.get(i);
                if (i < model.rate) {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_blue));
                } else {
                    img_star.setImageDrawable(getDrawable(R.drawable.ic_star_border_blue));
                }
            }
        });

        EditText lbl_comment = dialog.findViewById(R.id.txt_dialog_comment);

        Button btn_submit = dialog.findViewById(R.id.btn_dialog_submit);
        btn_submit.setOnClickListener(view -> {
            model.content = lbl_comment.getText().toString();
            if (model.content.length() == 0) {
                Toast.makeText(this, getString(R.string.dialog_comment_error), Toast.LENGTH_SHORT).show();
               return;
            }
            if (model.rate == 0) {
                Toast.makeText(this, getString(R.string.dialog_rate_error), Toast.LENGTH_SHORT).show();
                return;
            }
            if (AppUtils.isFBMode) {
                FireManager.addCommentToFirebase(model, () -> {
                    dialog.dismiss();
                    initWithdDatas();
                });
            } else {
                Map<String, String> params = new HashMap<>();
                params.put("spot_id", AppUtils.gProduct.id);
                params.put("usr_id", AppUtils.gUser.id);
                params.put("content", model.content);
                params.put("rate", String.valueOf(model.rate));

                APIManager.onAPIConnectionResponse(APIManager.LAO_SET_COMMNET, params
                        , ProductDetailActivity.this
                        , APIManager.APIMethod.POST, (obj, ret) -> {
                            try {
                                if (ret == 10000) {
                                    dialog.dismiss();
                                    initWithdDatas();
                                } else {
                                    String msg = obj.getString("msg");
                                    Toast.makeText(ProductDetailActivity.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(ProductDetailActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        });
            }
        });

        dialog.show();
    }

    public void onClickMoreLbl(View view) {
        AppUtils.showOtherActivity(this, CommentActivtiy.class, 0);
    }

    public void onClickLblLocation(View view) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", AppUtils.gProduct.lat, AppUtils.gProduct.log);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    public void onClickLblPhoneNumber1(View view) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + AppUtils.gProduct.phone1));
        startActivity(callIntent);
    }

    public void onClickLblPhoneNumber2(View view) {
        if (AppUtils.gProduct.phone2.length() == 0) {
            return;
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + AppUtils.gProduct.phone2));
        startActivity(callIntent);
    }

}
