package com.laodev.tourlao.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.kwabenaberko.openweathermaplib.constants.Units;
import com.kwabenaberko.openweathermaplib.implementation.OpenWeatherMapHelper;
import com.kwabenaberko.openweathermaplib.implementation.callbacks.ThreeHourForecastCallback;
import com.kwabenaberko.openweathermaplib.models.threehourforecast.ThreeHourForecast;
import com.kwabenaberko.openweathermaplib.models.threehourforecast.ThreeHourWeather;
import com.laodev.tourlao.R;
import com.laodev.tourlao.adapter.ADSliderAdapter;
import com.laodev.tourlao.adapter.BannerAdapter;
import com.laodev.tourlao.callback.APIListener;
import com.laodev.tourlao.model.ADModel;
import com.laodev.tourlao.model.CategoryModel;
import com.laodev.tourlao.model.ProvinceModel;
import com.laodev.tourlao.model.SponsorModel;
import com.laodev.tourlao.ui.CategoryUI;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mGoogleMap;

    private ListView lst_banner;
    private LinearLayout llt_category;

    private int banner_index = 0;
    private int change_index = 1;

    private ADSliderAdapter adSliderAdapter;

    private List<SponsorModel> mSponsors = new ArrayList<>();

    private ImageView img_sponsor;
    private int cnt_sponsor = -1;

    private CategoryUI.CategoryUICallback categoryUICallback = model -> {
        AppUtils.gCategoryModel = model;
        AppUtils.showOtherActivity(HomeActivity.this, CategoryActivity.class, 0);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        setTitle(getString(R.string.main_title));

        initUIView();
        initUIDatas();
    }

    private void initUIDatas() {
        APIManager.onAPIConnectionResponse(APIManager.LAO_ALL_CATE, null, this, APIManager.APIMethod.POST, (obj, ret) -> {
            try {
                if (ret == 10000) {
                    AppUtils.gCategories.clear();

                    JSONArray result = obj.getJSONArray("result");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject obj_cate = result.getJSONObject(i);
                        CategoryModel category = new CategoryModel(obj_cate);
                        AppUtils.gCategories.add(category);
                    }
                    initCategoryView();
                } else {
                    String msg = obj.getString("msg");
                    Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });

        APIManager.onAPIConnectionResponse(APIManager.LAO_ALL_PROVINCE, null, this, APIManager.APIMethod.POST, (obj, ret) -> {
            try {
                if (ret == 10000) {
                    AppUtils.gCategories.clear();

                    JSONArray result = obj.getJSONArray("result");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject obj_cate = result.getJSONObject(i);
                        ProvinceModel category = new ProvinceModel(obj_cate);
                        AppUtils.gProvinces.add(category);
                    }
                } else {
                    String msg = obj.getString("msg");
                    Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });

        APIManager.onAPIConnectionResponse(APIManager.LAO_ALL_AD, null, this, APIManager.APIMethod.POST, (obj, ret) -> {
            try {
                if (ret == 10000) {
                    AppUtils.gCategories.clear();

                    JSONArray result = obj.getJSONArray("result");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject obj_cate = result.getJSONObject(i);
                        ADModel ad = new ADModel(obj_cate);
                        AppUtils.gAdvers.add(ad);
                    }
                    adSliderAdapter.notifyDataSetChanged();
                } else {
                    String msg = obj.getString("msg");
                    Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });

        APIManager.onAPIConnectionResponse(APIManager.LAO_ALL_SPONSOR, null, this, APIManager.APIMethod.POST, (obj, ret) -> {
            try {
                if (ret == 10000) {
                    AppUtils.gCategories.clear();

                    JSONArray result = obj.getJSONArray("result");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject obj_cate = result.getJSONObject(i);
                        SponsorModel sponsorModel = new SponsorModel(obj_cate);
                        mSponsors.add(sponsorModel);
                    }
                    initWithSponsor();
                } else {
                    String msg = obj.getString("msg");
                    Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }

    private void initWithSponsor() {
        final Handler handler = new Handler();
        handler.postDelayed(this::showWithSponsor, 5000);
    }

    private void showWithSponsor() {
        cnt_sponsor++;
        YoYo.with(Techniques.FadeIn)
                .duration(700)
                .playOn(img_sponsor);
        SponsorModel model = mSponsors.get(cnt_sponsor % mSponsors.size());
        Picasso.with(this)
                .load(model.photo)
                .centerCrop()
                .fit()
                .into(img_sponsor);
        initWithSponsor();
    }

    private void initCategoryView() {
        for (CategoryModel categoryModel: AppUtils.gCategories) {
            CategoryUI categoryView = new CategoryUI(this);
            categoryView.setModel(categoryModel);
            categoryView.setCategoryUICallback(categoryUICallback);
            llt_category.addView(categoryView);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUIView() {
        SliderView sliderView = findViewById(R.id.sld_home_ad);

        adSliderAdapter = new ADSliderAdapter(this, AppUtils.gAdvers);

        sliderView.setSliderAdapter(adSliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.startAutoCycle();

        llt_category = findViewById(R.id.llt_home_category);

        TextView lbl_today = findViewById(R.id.lbl_home_today);
        TextView lbl_tomorrow = findViewById(R.id.lbl_home_tomorrow);

        OpenWeatherMapHelper helper = new OpenWeatherMapHelper(getString(R.string.weather_api));
        helper.setUnits(Units.METRIC);
        helper.getThreeHourForecastByCityName("Vientiane", new ThreeHourForecastCallback() {
            @Override
            public void onSuccess(ThreeHourForecast threeHourForecast) {
                ThreeHourWeather weather1 = threeHourForecast.getList().get(1);
                ThreeHourWeather weather2 = threeHourForecast.getList().get(3);
                ThreeHourWeather weather3 = threeHourForecast.getList().get(9);
                ThreeHourWeather weather4 = threeHourForecast.getList().get(11);
                lbl_today.setText((int)weather1.getMain().getTemp() + " \u2103  " + (int)weather2.getMain().getTemp() + " \u2103");
                lbl_tomorrow.setText((int)weather3.getMain().getTemp() + " \u2103  " + (int)weather4.getMain().getTemp() + " \u2103");
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });

        BannerAdapter bannerAdapter = new BannerAdapter(this);
        lst_banner = findViewById(R.id.lst_home_banner);
        lst_banner.setAdapter(bannerAdapter);
        lst_banner.setOnItemClickListener((parent, view, position, id) -> {
            onShowBannerDetail(position);
        });
        lst_banner.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                return true; // Indicates that this has been handled by you and will not be forwarded further.
            }
            return false;
        });
        onSliderBanner();

        img_sponsor = findViewById(R.id.img_main_sponsor);
        img_sponsor.setOnClickListener(view -> {
            SponsorModel model = mSponsors.get(cnt_sponsor % mSponsors.size());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.link));
            startActivity(browserIntent);
        });
    }

    private void onShowBannerDetail(int position) {
        String url = "";
        switch (position) {
            case 0:
                url = "http://www.etllao.com/";
                break;
            case 1:
                url = "http://laoplazahotel.com/";
                break;
            case 3:
                url = "https://laotel.com/";
                break;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void onSliderBanner() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            //Do something after 1500ms
            banner_index = banner_index + change_index;
            if (banner_index == 2) {
                change_index = -1;
            }
            if (banner_index == 0) {
                change_index = 1;
            }
            lst_banner.smoothScrollToPosition(banner_index);
            onSliderBanner();
        }, 5000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return false;
            case R.id.menu_home_profile:
                AppUtils.showOtherActivity(this, ProfileActivity.class, 0);
                return false;
            case R.id.menu_home_facebook:
            case R.id.menu_home_twitter:
                String message = "My Tourlao app Share";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, getString(R.string.app_name)));
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.common_warning));
        alertDialogBuilder
                .setMessage(getString(R.string.login_exist_message))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.common_yes), (dialog, id) -> {
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                })

                .setNegativeButton(getString(R.string.common_no), (dialog, id) -> dialog.cancel());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        }
        else {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(HomeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

}
