package com.laodev.tourlao.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.laodev.tourlao.R;
import com.laodev.tourlao.adapter.CommentAdapter;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentActivtiy extends AppCompatActivity {

    private List<CommentModel> mComments = new ArrayList<>();
    private CommentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        setTitle(R.string.commen_comment);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);

        initWithUIView();
        initWithData();
    }

    private void initWithData() {
        if (AppUtils.isFBMode) {
            FireManager.getCommentFromFirebase(new FireManager.GetCommentCallback() {
                @Override
                public void onCallbackSuccess(List<CommentModel> comments) {
                    mComments.clear();
                    for (CommentModel comment: comments) {
                        if (comment.postid.equals(AppUtils.gProduct.id)) {
                            mComments.add(comment);
                        }
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onCallbackFailed(String error) {
                    //
                }
            });
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("spot_id", AppUtils.gProduct.id);
            APIManager.onAPIConnectionResponse(APIManager.LAO_GET_COMMNET
                    , params, CommentActivtiy.this
                    , APIManager.APIMethod.POST, (obj, ret) -> {
                        try {
                            if (ret == 10000) {
                                mComments.clear();

                                JSONArray objArray = obj.getJSONArray("result");
                                for (int i = 0; i < objArray.length(); i++) {
                                    JSONObject obj_model = objArray.getJSONObject(i);
                                    CommentModel model = new CommentModel();
                                    model.initWithJSON(obj_model);
                                    mComments.add(model);
                                }
                                adapter.notifyDataSetChanged();
                            } else {
                                String msg = obj.getString("msg");
                                Toast.makeText(CommentActivtiy.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(CommentActivtiy.this, e.toString(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
        }
    }

    private void initWithUIView() {
        adapter = new CommentAdapter(this, mComments);

        ListView lst_comment = findViewById(R.id.lst_comment);
        lst_comment.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

}
