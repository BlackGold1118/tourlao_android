package com.laodev.tourlao.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.laodev.tourlao.R;
import com.laodev.tourlao.adapter.DialogCategoryAdapter;
import com.laodev.tourlao.adapter.DialogProvinceAdapter;
import com.laodev.tourlao.model.ProductModel;
import com.laodev.tourlao.model.ProvinceModel;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;

public class AddProductActivity extends AppCompatActivity {

    private ArrayList<CustomUri> ary_uris = new ArrayList<>();
    private ArrayList<ImageView> ary_add_imgs = new ArrayList<>();
    private ArrayList<ImageView> ary_show_imgs = new ArrayList<>();

    private int sel_index;
    private int cnt_imgs;
    private String sel_category;
    private String sel_province;

    private TextInputEditText txt_name, txt_lname, txt_email, txt_category, txt_province, txt_price, txt_phone1, txt_phone2, txt_lat, txt_log;
    private TextInputLayout tll_name, tll_lname, tll_email, tll_category, tll_province, tll_price, tll_phone1, tll_phone2, tll_lat, tll_log;
    private EditText txt_desc, txt_ldesc;

    private Dialog dialog;
    private ProgressDialog progressDialog;

    private ProductModel productModel = new ProductModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.getWindow().setStatusBarColor(this.getColor(R.color.colorBackground));
        } else {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackground));
        }

        setTitle(getString(R.string.add_product_title));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);

        initUIView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUIView() {
        dialog = new Dialog(this);

        ImageView img_add_1 = findViewById(R.id.img_add_btn_1);
        ImageView img_add_2 = findViewById(R.id.img_add_btn_2);
        ImageView img_add_3 = findViewById(R.id.img_add_btn_3);
        ImageView img_add_4 = findViewById(R.id.img_add_btn_4);
        ImageView img_add_5 = findViewById(R.id.img_add_btn_5);
        ImageView img_add_6 = findViewById(R.id.img_add_btn_6);
        ImageView img_add_7 = findViewById(R.id.img_add_btn_7);
        ImageView img_add_8 = findViewById(R.id.img_add_btn_8);
        ImageView img_add_9 = findViewById(R.id.img_add_btn_9);
        ary_add_imgs.add(img_add_1);
        ary_add_imgs.add(img_add_2);
        ary_add_imgs.add(img_add_3);
        ary_add_imgs.add(img_add_4);
        ary_add_imgs.add(img_add_5);
        ary_add_imgs.add(img_add_6);
        ary_add_imgs.add(img_add_7);
        ary_add_imgs.add(img_add_8);
        ary_add_imgs.add(img_add_9);

        ImageView img_show_1 = findViewById(R.id.img_add_show_1);
        ImageView img_show_2 = findViewById(R.id.img_add_show_2);
        ImageView img_show_3 = findViewById(R.id.img_add_show_3);
        ImageView img_show_4 = findViewById(R.id.img_add_show_4);
        ImageView img_show_5 = findViewById(R.id.img_add_show_5);
        ImageView img_show_6 = findViewById(R.id.img_add_show_6);
        ImageView img_show_7 = findViewById(R.id.img_add_show_7);
        ImageView img_show_8 = findViewById(R.id.img_add_show_8);
        ImageView img_show_9 = findViewById(R.id.img_add_show_9);
        ary_show_imgs.add(img_show_1);
        ary_show_imgs.add(img_show_2);
        ary_show_imgs.add(img_show_3);
        ary_show_imgs.add(img_show_4);
        ary_show_imgs.add(img_show_5);
        ary_show_imgs.add(img_show_6);
        ary_show_imgs.add(img_show_7);
        ary_show_imgs.add(img_show_8);
        ary_show_imgs.add(img_show_9);

        tll_name = findViewById(R.id.tll_add_name);
        tll_lname = findViewById(R.id.tll_add_lname);
        tll_email = findViewById(R.id.tll_add_email);
        tll_category = findViewById(R.id.tll_add_category);
        tll_province = findViewById(R.id.tll_add_province);
        tll_lat = findViewById(R.id.tll_add_lat);
        tll_log= findViewById(R.id.tll_add_log);
        tll_price = findViewById(R.id.tll_add_price);
        tll_phone1 = findViewById(R.id.tll_add_phone1);
        tll_phone2 = findViewById(R.id.tll_add_phone2);

        txt_name = findViewById(R.id.txt_add_name);
        txt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_name.setHelperText("");
            }
        });

        txt_lname = findViewById(R.id.txt_add_lname);
        txt_lname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_lname.setHelperText("");
            }
        });

        txt_email = findViewById(R.id.txt_add_email);
        txt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_email.setHelperText("");
            }
        });

        txt_category = findViewById(R.id.txt_add_category);
        txt_category.setOnTouchListener((view, motionEvent) -> {
            onShowCategoryListModal();
            return true;
        });

        txt_province = findViewById(R.id.txt_add_province);
        txt_province.setOnTouchListener((view, motionEvent) -> {
            onShowProvinceListModal();
            return true;
        });

        txt_lat = findViewById(R.id.txt_add_lat);
        txt_lat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_lat.setHelperText("");
            }
        });

        txt_log = findViewById(R.id.txt_add_log);
        txt_log.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_log.setHelperText("");
            }
        });

        txt_price = findViewById(R.id.txt_add_price);
        txt_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_price.setHelperText("");
            }
        });

        txt_phone1 = findViewById(R.id.txt_add_phone1);
        txt_phone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_phone1.setHelperText("");
            }
        });

        txt_phone2 = findViewById(R.id.txt_add_phone2);
        txt_phone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                tll_phone2.setHelperText("");
            }
        });

        txt_desc = findViewById(R.id.txt_add_desc);
        txt_ldesc = findViewById(R.id.txt_add_ldesc);

        reloadImgArray();

        if (true) {
            txt_name.setText("Test Name");
            txt_lname.setText("Test Lao Name");
            txt_email.setText("bgold118@gmail.com");
            txt_lat.setText("16.235584");
            txt_log.setText("102.356983");
            txt_price.setText("12000");
            txt_phone1.setText("+8562096227257");
            txt_phone2.setText("+8562096227257");
            txt_desc.setText("Test Description");
            txt_ldesc.setText("Test Lao Description");
        }
    }

    private void onShowProvinceListModal() {
        if (dialog.isShowing()) {
            return;
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.dialog_province);
        dialog.setTitle(null);
        dialog.setCanceledOnTouchOutside(false);

        DialogProvinceAdapter adapter = new DialogProvinceAdapter(this);
        adapter.setDialogProvinceCallback(model -> {
            sel_province = model.id;
            txt_province.setText(model.name);
            dialog.dismiss();
        });

        ListView lst_province = dialog.findViewById(R.id.lst_dialog_province);
        lst_province.setAdapter(adapter);
        dialog.show();
    }

    private void onShowCategoryListModal() {
        if (dialog.isShowing()) {
            return;
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.dialog_category);
        dialog.setTitle(null);
        dialog.setCanceledOnTouchOutside(false);

        DialogCategoryAdapter adapter = new DialogCategoryAdapter(this);
        adapter.setDialogCategoryCallback(model -> {
            sel_category = model.id;
            txt_category.setText(model.name);
            dialog.dismiss();
        });

        ListView lst_category = dialog.findViewById(R.id.lst_dialog_category);
        lst_category.setAdapter(adapter);
        dialog.show();
    }

    public void onClickBtnAddProduct(View view) {
        if (ary_uris.size() == 0) {
            Toast.makeText(this, getString(R.string.add_error_image), Toast.LENGTH_SHORT).show();
            return;
        }

        String name = txt_name.getText().toString();
        if (name.length() == 0) {
            tll_name.setHelperText(getString(R.string.register_name_wrong));
            return;
        }
        String lname = txt_lname.getText().toString();
        if (lname.length() == 0) {
            tll_lname.setHelperText(getString(R.string.register_name_wrong));
            return;
        }
        String email = txt_email.getText().toString();
        if (email.length() == 0) {
            tll_email.setHelperText(getString(R.string.register_email_empty));
            return;
        }
        if (!(email.contains("@") || email.contains(".com"))) {
            tll_email.setHelperText(getString(R.string.register_email_wrong));
            return;
        }
        String category = txt_category.getText().toString();
        if (category.length() == 0) {
            tll_category.setHelperText(getString(R.string.add_category_wrong));
            return;
        }
        String province = txt_province.getText().toString();
        if (province.length() == 0) {
            tll_province.setHelperText(getString(R.string.add_province_wrong));
            return;
        }
        String lat = txt_lat.getText().toString();
        if (lat.length() == 0) {
            tll_lat.setHelperText(getString(R.string.add_lat_wrong));
            return;
        }
        String log = txt_log.getText().toString();
        if (log.length() == 0) {
            tll_log.setHelperText(getString(R.string.add_log_wrong));
            return;
        }
        String price = txt_price.getText().toString();
        if (price.length() == 0) {
            tll_price.setHelperText(getString(R.string.add_province_wrong));
            return;
        }
        String phone1 = txt_phone1.getText().toString();
        if (phone1.length() == 0) {
            tll_phone1.setHelperText(getString(R.string.add_phone_wrong));
            return;
        }
        String phone2 = txt_phone2.getText().toString();
        String desc1 = txt_desc.getText().toString();
        if (desc1.length() == 0) {
            Toast.makeText(this, getString(R.string.add_desc_wrong), Toast.LENGTH_SHORT).show();
            return;
        }
        String desc2 = txt_ldesc.getText().toString();
        if (desc2.length() == 0) {
            Toast.makeText(this, getString(R.string.add_ldesc_wrong), Toast.LENGTH_SHORT).show();
            return;
        }

        productModel = new ProductModel();
        productModel.name = name;
        productModel.name_la = lname;
        productModel.email = email;
        productModel.category = sel_category;
        productModel.province = sel_province;
        productModel.lat = Float.valueOf(lat);
        productModel.log = Float.valueOf(log);
        productModel.price = price;
        productModel.phone1 = phone1;
        productModel.phone2 = phone2;
        productModel.detail = desc1;
        productModel.detail_la = desc2;
        addProductToFirebase();
    }

    private void addProductToFirebase() {
        cnt_imgs = 0;
        progressDialog = AppUtils.onShowProgressDialog(this, getResources().getString(R.string.common_server_connect), false);
        uploadAllImages();
    }

    private void uploadAllImages() {
        FireManager.uploadProductImgToFirebase(ary_uris.get(cnt_imgs).mUri, url -> {
            productModel.photo.add(url);
            cnt_imgs++;
            if (cnt_imgs == ary_uris.size()) {
                uploadProduct();
            } else {
                uploadAllImages();
            }
        });
    }

    private void uploadProduct() {
        FireManager.addProductToFirebase(productModel, () -> {
            Toast.makeText(AddProductActivity.this, getString(R.string.common_upload_success), Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        });
    }

    public void onClickAddBtn(View view) {
        sel_index = Integer.parseInt(view.getTag().toString());
        ImageView img = ary_add_imgs.get(sel_index);
        if (!isCheckAddImage(sel_index)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 10001);
                return;
            }
            img.setImageResource(R.drawable.ic_remove_circle_blue);
            CropImage.activity()
                    .setMaxCropResultSize(AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT)
                    .setAutoZoomEnabled(false)
                    .start(this);
        } else {
            for (CustomUri uri : ary_uris) {
                if (uri.mIndex == sel_index) {
                    ary_uris.remove(uri);
                    img.setImageResource(R.drawable.ic_add_circle_blue);
                    reloadImgArray();
                    break;
                }
            }
        }
    }

    private boolean isCheckAddImage(int index) {
        for (CustomUri uri: ary_uris) {
            if (uri.mIndex == index) {
                return true;
            }
        }
        return false;
    }

    private void reloadImgArray() {
        for (int i = 0; i < 9; i++) {
            ImageView imageView = ary_show_imgs.get(i);
            imageView.setVisibility(View.INVISIBLE);
        }
        for (CustomUri uri : ary_uris) {
            ImageView imageView = ary_show_imgs.get(uri.mIndex);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageURI(uri.mUri);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                CustomUri uri = new CustomUri(sel_index, resultUri);
                ary_uris.add(uri);
                reloadImgArray();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 10001: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageView img = ary_add_imgs.get(sel_index);
                    img.setImageResource(R.drawable.ic_remove_circle_blue);
                    CropImage.activity()
                            .setMaxCropResultSize(AppUtils.IMAGE_WIDTH, AppUtils.IMAGE_HEIGHT)
                            .setAutoZoomEnabled(false)
                            .start(this);
                } else {
                    Toast.makeText(this, getString(R.string.common_permission_denied), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private class CustomUri {

        private int mIndex;
        private Uri mUri;

        CustomUri(int index, Uri uri) {
            mIndex = index;
            mUri = uri;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }


}
