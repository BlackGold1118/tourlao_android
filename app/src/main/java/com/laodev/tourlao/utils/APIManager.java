package com.laodev.tourlao.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;

import com.laodev.tourlao.R;

import com.laodev.tourlao.callback.APIListener;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class APIManager {

    public static final String SHARE_PATH = "Tour Lao";

    private static final String SERVER_URL = "https://tourlao.daxiao-itdev.com/";

    private static final String BASE_URL = SERVER_URL +  "Backend/";
    public static final String DOWNLOAD_URL = SERVER_URL +  "download/";
    public static final String UPLOAD_URL = SERVER_URL +  "uploads/";

    public static final String LAO_LOGIN = BASE_URL + "tourlao_login";
    public static final String LAO_ALL_CATE = BASE_URL + "tourlao_get_all_cat";
    public static final String LAO_ALL_PROVINCE = BASE_URL + "tourlao_get_all_province";
    public static final String LAO_ALL_AD = BASE_URL + "tourlao_get_all_ad";
    public static final String LAO_ALL_SPONSOR = BASE_URL + "tourlao_get_all_sponsor";
    public static final String LAO_CATEGORY_SPOT = BASE_URL + "tourlao_cat_spot";
    public static final String LAO_SET_LIKE = BASE_URL + "tourlao_set_like";
    public static final String LAO_GET_COMMNET = BASE_URL + "tourlao_get_comment";
    public static final String LAO_SET_COMMNET = BASE_URL + "tourlao_set_comment";
    public static final String LAO_UP_USER = BASE_URL + "tourlao_up_user";
    public static final String LAO_CH_PASS = BASE_URL + "tourlao_ch_pass";
    public static final String LAO_SET_AVATAR = BASE_URL + "tourlao_set_avatar";

    public enum APIMethod {
        GET, POST
    }

    public static void onAPIConnectionResponse (String url
            , Map<String, String> params
            , Activity activity
            , APIMethod method
            , APIListener apiResponse)
    {
        ProgressDialog dialog = ProgressDialog.show(activity
                , activity.getString(R.string.progress_title)
                , activity.getString(R.string.progress_detail));

        if (method == APIMethod.POST) {
            OkHttpUtils.post().url(url)
                    .params(params)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(okhttp3.Call call, Exception e, int id) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            Toast.makeText(activity, R.string.alert_error_internet_detail, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            try {
                                JSONObject obj = new JSONObject(response);
                                int ret = obj.getInt("ret");
                                apiResponse.onEventCallBack(obj, ret);
                            } catch (JSONException e) {
                                Toast.makeText(activity, R.string.alert_server_error_detail, Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    });
        } else {
            OkHttpUtils.get().url(url)
                    .params(params)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(okhttp3.Call call, Exception e, int id) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            Toast.makeText(activity, R.string.alert_error_internet_detail, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            try {
                                JSONObject obj = new JSONObject(response);
                                int ret = obj.getInt("ret");
                                apiResponse.onEventCallBack(obj, ret);
                            } catch (JSONException e) {
                                Toast.makeText(activity, R.string.alert_server_error_detail, Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    });
        }
    }

}
