package com.laodev.tourlao.utils;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.facebook.Profile;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.model.ProductModel;
import com.laodev.tourlao.model.UserModel;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FireManager {

    private static final FirebaseDatabase database = FirebaseDatabase.getInstance();

    private static final FirebaseStorage storage = FirebaseStorage.getInstance();

    private static final DatabaseReference mainRef = database.getReference();

    private static final DatabaseReference usersRef = mainRef.child("Users");
    private static final DatabaseReference productsRef = mainRef.child("Products");
    private static final DatabaseReference commentsRef = mainRef.child("Comments");
    private static final DatabaseReference likesRef = mainRef.child("Likes");

    //is this user is logged in
    public static boolean isLoggedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    //get this user's uid
    public static String getUid() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            return FirebaseAuth.getInstance().getCurrentUser().getUid();

        return null;
    }

    // Firebase Managment user's Informations

    static public void getUserFromFirebase(String uid, final UserInfoCallback userInfoCallback) {
        DatabaseReference ref = usersRef.child(uid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserModel user = dataSnapshot.getValue(UserModel.class);
                userInfoCallback.onGetUserCallback(user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    static public void updateUserFromFirebase(UserModel user, final UserInfoCallback userInfoCallback) {
        DatabaseReference ref = usersRef.child(user.id);
        ref.setValue(user).addOnSuccessListener(aVoid -> userInfoCallback.onGetUserCallback(user));
    }

    public interface UserInfoCallback {
        void onGetUserCallback(UserModel user);
    }

    public static void setUserAvatarToFirebase(final Uri imagePath, final UploadImageCallback uploadUserAvatarCallback) {
        String fileName = "avatar/" + UUID.randomUUID().toString();
        final StorageReference storageRef = storage.getReference().child(fileName);
        UploadTask uploadTask = storageRef.putFile(imagePath);
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            storageRef.getDownloadUrl().addOnSuccessListener(downloadPhotoUrl -> {
                String url = downloadPhotoUrl.toString();
                uploadUserAvatarCallback.onCallbackFromFirebase(url);
            });
        });
    }

    public interface UploadImageCallback {
        void onCallbackFromFirebase(String url);
    }

    public static void changePassword(String email, String opass, String npass, final ChangePasswordCallback callback) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        AuthCredential credential = EmailAuthProvider
                .getCredential(email, opass);
        user.reauthenticate(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        user.updatePassword(npass).addOnCompleteListener(task1 -> {
                            if (task1.isSuccessful()) {
                                callback.onCallbackSuccess();
                            } else {
                                callback.onCallbackFailed();
                            }
                        });
                    } else {
                        callback.onCallbackIncorrectPass();
                    }
                });
    }

    public interface ChangePasswordCallback {
        void onCallbackSuccess();
        void onCallbackIncorrectPass();
        void onCallbackFailed();
    }

    public static void getUserInfoByFBLogin(String userid, Profile profile, FBUserInfoCallback userInfoCallback) {
        DatabaseReference ref = usersRef.child(userid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserModel user = dataSnapshot.getValue(UserModel.class);
                if (user == null) {
                    UserModel newUser = new UserModel();
                    newUser.id = userid;
                    newUser.name = profile.getName();
                    newUser.photo = profile.getProfilePictureUri(256, 256).toString();

                    ref.setValue(newUser).addOnSuccessListener(aVoid -> userInfoCallback.onGetUserCallback(newUser));
                } else {
                    userInfoCallback.onGetUserCallback(user);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //
            }
        });
    }

    public interface FBUserInfoCallback {
        void onGetUserCallback(UserModel user);
    }

//    Product Part
    public static void uploadProductImgToFirebase(final Uri imagePath, final UploadImageCallback uploadImageCallback) {
        String fileName = "product/" + UUID.randomUUID().toString();
        final StorageReference storageRef = storage.getReference().child(fileName);
        UploadTask uploadTask = storageRef.putFile(imagePath);
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            storageRef.getDownloadUrl().addOnSuccessListener(downloadPhotoUrl -> {
                String url = downloadPhotoUrl.toString();
                uploadImageCallback.onCallbackFromFirebase(url);
            });
        });
    }

    public static void addProductToFirebase(ProductModel model, final AddProductCallback addProductCallback) {
        model.id = productsRef.push().getKey();
        DatabaseReference ref = productsRef.child(model.id);
        ref.setValue(model).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                addProductCallback.onCallbackSuccess();
            }
        });
    }

    public interface AddProductCallback {
        void onCallbackSuccess();
    }

    public static void getProductFromFirebase(String categoryId, final GetProductCallback productCallback) {
        productsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<ProductModel> postList = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    ProductModel post = postSnapshot.getValue(ProductModel.class);
                    if (post.category.equals(categoryId)) {
                        postList.add(post);
                    }
                }
                productCallback.onCallbackSuccess(postList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface GetProductCallback {
        void onCallbackSuccess(List<ProductModel> productModels);
    }

//    Comment Part

    public static void getCommentFromFirebase(GetCommentCallback commentCallback) {
        commentsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<CommentModel> comments = new ArrayList<>();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    CommentModel comment = snapshot.getValue(CommentModel.class);
                    comments.add(0, comment);
                }
                commentCallback.onCallbackSuccess(comments);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                commentCallback.onCallbackFailed(databaseError.getDetails());
            }
        });
    }

    public interface GetCommentCallback {
        void onCallbackSuccess(List<CommentModel> comments);
        void onCallbackFailed(String error);
    }

    public static void addCommentToFirebase(CommentModel model, final AddCommentCallback callback) {
        model.id = commentsRef.push().getKey();
        commentsRef.child(model.id).setValue(model).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                callback.onCallbackSuccess();
            }
        });
    }

    public interface AddCommentCallback {
        void onCallbackSuccess();
    }

//    Like Part
    public static void getLikeFromFirebase(String productId, final GetLikeCallback getLikeCallback) {
        likesRef.child(productId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> userLists = new ArrayList<>();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    String userid = snapshot.getValue(String.class);
                    userLists.add(userid);
                }
                getLikeCallback.onCallbackSuccess(userLists);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                getLikeCallback.onCallbackFailed(databaseError.getDetails());
            }
        });
    }

    public interface GetLikeCallback {
        void onCallbackSuccess(List<String> users);
        void onCallbackFailed(String error);
    }

    public static void changeLikeFromFirebase(String productId, List<String> users, final ChangeLikeCallback likeCallback) {
        likesRef.child(productId).setValue(users).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                likeCallback.onCallbackSuccess();
            }
        });
    }

    public interface ChangeLikeCallback {
        void onCallbackSuccess();
    }

}
