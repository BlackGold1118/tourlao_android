package com.laodev.tourlao.model;

import com.laodev.tourlao.utils.APIManager;

import org.json.JSONException;
import org.json.JSONObject;

public class SponsorModel {

    public String id = "";
    public String photo = "";
    public String link = "";


    public SponsorModel() {
        id = "";
        link = "";
        photo = "";
    }

    public SponsorModel(JSONObject obj) {
        try {
            id  = obj.getString("sponsor_id");
            link = obj.getString("sponsor_link");
            photo = APIManager.UPLOAD_URL + "sponsor/" + obj.getString("sponsor_photo");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
