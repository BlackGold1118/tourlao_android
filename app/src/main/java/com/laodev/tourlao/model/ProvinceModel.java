package com.laodev.tourlao.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ProvinceModel {

    public String id = "";
    public String name = "";
    public String desc = "";
    public String img = "";

    public String created_at = "";
    public String updated_at = "";

    public float lat = 0;
    public float log = 0;

    public ProvinceModel(JSONObject json) {
        try {
            id = json.getString("loc_id");
            name = json.getString("loc_name");
            desc = json.getString("loc_summary");
            img = json.getString("loc_thumb");

            lat = json.getLong("loc_lat");
            log = json.getLong("loc_lon");

            created_at = json.getString("created_at");
            updated_at = json.getString("updated_at");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
