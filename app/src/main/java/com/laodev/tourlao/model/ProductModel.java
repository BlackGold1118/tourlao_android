package com.laodev.tourlao.model;

import android.content.Context;

import com.laodev.tourlao.utils.APIManager;
import com.laodev.tourlao.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductModel {

    public String id = "";
    public List<String> photo = new ArrayList<>();

    public String name = "";
    public String name_la = "";
    public String email = "";
    public String phone1 = "";
    public String phone2 = "";
    public String detail = "";
    public String detail_la = "";
    public String category = "";
    public String province = "";
    public String price = "";

    public int cnt_comment = 0;

    public boolean isLike = false;
    public int cnt_like = 0;

    public float lat = (float) 0.0;
    public float log = (float) 0.0;

    public boolean isContainSearchKey(Context context, String key) {
        if (key.length() == 0) {
            return true;
        }
        if (name.contains(key)) {
            return true;
        }
        if (name_la.contains(key)) {
            return true;
        }
        ProvinceModel provinceModel = AppUtils.getProvinceByID(province);
        if (provinceModel.name.contains(key)) {
            return true;
        }
        return price.contains(key);
    }

    public void initWithJSON(JSONObject obj) {
        try {
            id = obj.getString("spot_id");
            name = obj.getString("spot_name");
            name_la = obj.getString("spot_name_la");
            email = obj.getString("con_email");
            phone1 = obj.getString("con_tel");
            phone2 = obj.getString("con_fax");
            detail = obj.getString("spot_desc");
            detail_la = obj.getString("spot_desc_la");
            category = obj.getString("sc_id");
            province = obj.getString("loc_id");
            price = obj.getString("spot_price");
            lat = (float) obj.getDouble("spot_lat");
            log = (float) obj.getDouble("spot_lon");

            isLike = obj.getBoolean("is_like");
            cnt_like = obj.getInt("cnt_like");

            cnt_comment = obj.getInt("cnt_comment");

//            JSONArray img_ary = obj.getJSONArray("spot_imgs");
//            for (int i = 0; i <  img_ary.length(); i++) {
//                JSONObject img_obj = img_ary.getJSONObject(i);
//                photo.add(APIManager.UPLOAD_URL + img_obj.getString("name"));
//            }
            String img_ary = obj.getString("spot_imgs");
            String[] imgs = img_ary.split("/");
            for (String img: imgs) {
                photo.add(APIManager.UPLOAD_URL + "spot/" + img);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
