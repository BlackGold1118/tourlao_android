package com.laodev.tourlao.model;

import com.laodev.tourlao.utils.APIManager;

import org.json.JSONException;
import org.json.JSONObject;

public class ADModel {

    public String id = "";
    public String name = "";
    public String desc = "";
    public String link = "";
    public String photo = "";

    public ADModel() {
        id = "";
        name = "";
        desc = "";
        link = "";
        photo = "";
    }

    public ADModel(JSONObject obj) {
        try {
            id  = obj.getString("ad_id");
            name = obj.getString("ad_name");
            desc = obj.getString("ad_desc");
            link = obj.getString("ad_link");
            photo = APIManager.UPLOAD_URL + "ad/" + obj.getString("ad_photo");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
