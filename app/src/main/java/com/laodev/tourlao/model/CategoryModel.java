package com.laodev.tourlao.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CategoryModel {

    public String id = "";
    public String name = "";
    public String name_la = "";
    public String imgurl = "";
    public String created_at = "";
    public String updated_at = "";

    public CategoryModel() {
        id = "";
        name = "";
        name_la = "";
        imgurl = "";
        created_at = "";
        updated_at = "";
    }

    public CategoryModel(JSONObject json) {
        try {
            id = json.getString("sc_id");
            name = json.getString("sc_name");
            name_la = json.getString("sc_name_la");
            imgurl = json.getString("sc_icon");
            created_at = json.getString("created_at");
            updated_at = json.getString("updated_at");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
