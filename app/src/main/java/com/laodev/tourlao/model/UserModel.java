package com.laodev.tourlao.model;

import com.laodev.tourlao.utils.APIManager;

import org.json.JSONException;
import org.json.JSONObject;

public class UserModel {

    public String id = "";
    public String uid = "";
    public int type = 0;
    public String email = "";
    public String name = "";
    public String photo = "";
    public String country = "";

    public void initWithJSON(JSONObject obj) {
        try {
            id = obj.getString("usr_id");
            uid = obj.getString("usr_uid");
            type = obj.getInt("usr_type");
            email = obj.getString("usr_email");
            name = obj.getString("usr_name");
            photo = APIManager.UPLOAD_URL + obj.getString("usr_photo");
            country = obj.getString("usr_country");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
