package com.laodev.tourlao.model;

import com.laodev.tourlao.utils.APIManager;

import org.json.JSONException;
import org.json.JSONObject;

public class CommentModel {
    public String id;
    public String commentid;
    public String content;
    public String date = "";
    public int rate;

    public String postid;

    public String userid;
    public String usr_img;
    public String usr_name;
    public String usr_country;

    public void initWithJSON(JSONObject obj) {
        try {
            id = obj.getString("id");
            content = obj.getString("content");
            date = obj.getString("created_at");
            rate = obj.getInt("rate");

            postid = obj.getString("spot_id");

            userid = obj.getString("usr_id");
            usr_img = obj.getString("usr_img");
            if (usr_img.equals("")) {
                usr_img = "User Image";
            }
            usr_img = APIManager.UPLOAD_URL + usr_img;
            usr_name = obj.getString("usr_name");
            usr_country = obj.getString("usr_country");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
