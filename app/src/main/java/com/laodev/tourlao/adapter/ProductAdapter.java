package com.laodev.tourlao.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.tourlao.R;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.model.ProductModel;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class ProductAdapter extends BaseAdapter {

    private Context mContext;
    private List<ProductModel> mProducts;
    private List<CommentModel> mComments;

    private ProductCellCallback productCellCallback;

    public ProductAdapter(Context context, List<ProductModel> models, List<CommentModel> comments) {
        mContext = context;
        mProducts = models;
        mComments = comments;
    }

    @Override
    public int getCount() {
        return mProducts.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.item_product, null);

        ProductModel model = mProducts.get(i);

        ImageView img_product = view.findViewById(R.id.img_product);
        if (model.photo.size() > 0) {
            Picasso.with(mContext).load(model.photo.get(0)).fit().centerCrop()
                    .placeholder(R.drawable.ic_image_blue)
                    .error(R.drawable.ic_image_blue)
                    .into(img_product, null);
        }

        TextView lbl_count = view.findViewById(R.id.lbl_product_count);
        if (model.photo.size() > 1) {
            lbl_count.setVisibility(View.VISIBLE);
            lbl_count.setText("+ " + (model.photo.size() - 1));
        } else {
            lbl_count.setVisibility(View.GONE);
        }

        TextView lbl_title = view.findViewById(R.id.lbl_product_title);
        String localization = Locale.getDefault().getLanguage();
        if (localization.equals("lo")) {
            lbl_title.setText(model.name_la);
        } else {
            lbl_title.setText(model.name);
        }

        ImageView img_like = view.findViewById(R.id.img_product_like);

        TextView lbl_like = view.findViewById(R.id.lbl_product_like);
        lbl_like.setOnClickListener(view12 -> productCellCallback.onClickLikeLabelCallback(model));
        if (AppUtils.isFBMode) {
            FireManager.getLikeFromFirebase(model.id, new FireManager.GetLikeCallback() {
                @Override
                public void onCallbackSuccess(List<String> users) {
                    lbl_like.setText(String.valueOf(users.size()));
                    if (users.contains(AppUtils.gUser.id)) {
                        img_like.setImageDrawable(mContext.getDrawable(R.drawable.ic_like_blue));
                    } else {
                        img_like.setImageDrawable(mContext.getDrawable(R.drawable.ic_like_gray));
                    }
                    img_like.setOnClickListener(view12 -> productCellCallback.onClickLikeImageCallback(model, users));
                }

                @Override
                public void onCallbackFailed(String error) {
                    //
                }
            });
        } else {
            if (model.isLike) {
                img_like.setImageDrawable(mContext.getDrawable(R.drawable.ic_like_blue));
            } else {
                img_like.setImageDrawable(mContext.getDrawable(R.drawable.ic_like_gray));
            }
            lbl_like.setText(String.valueOf(model.cnt_like));
        }

        ImageView img_comment = view.findViewById(R.id.img_product_comment);
        img_comment.setOnClickListener(view13 -> productCellCallback.onClickCommentCallback(model));

        TextView lbl_comment = view.findViewById(R.id.lbl_product_comment);
        lbl_comment.setOnClickListener(view12 -> productCellCallback.onClickCommentCallback(model));
        int comment_cnt = 0;
        if (AppUtils.isFBMode) {
            for (CommentModel commentModel: mComments) {
                if (commentModel.postid.equals(model.id)) {
                    comment_cnt++;
                }
            }
            lbl_comment.setText(String.valueOf(comment_cnt));
        } else {
            lbl_comment.setText(String.valueOf(model.cnt_comment));
        }

        ImageView img_share = view.findViewById(R.id.img_product_share);
        img_share.setOnClickListener(v -> productCellCallback.onClickShareCallback(model));

        TextView lbl_detail = view.findViewById(R.id.lbl_product_description);
        if (localization.equals("lo")) {
            lbl_detail.setText(model.detail_la);
        } else {
            lbl_detail.setText(model.detail);
        }

        view.setOnClickListener(view1 -> productCellCallback.onClickProductCallback(model));

        return view;
    }

    public void setProductCellCallback(ProductCellCallback productCellCallback) {
        this.productCellCallback = productCellCallback;
    }

    public interface ProductCellCallback {
        void onClickProductCallback(ProductModel model);
        void onClickLikeImageCallback(ProductModel model, List<String> users);
        void onClickLikeLabelCallback(ProductModel model);
        void onClickCommentCallback(ProductModel model);
        void onClickShareCallback(ProductModel model);
    }

}
