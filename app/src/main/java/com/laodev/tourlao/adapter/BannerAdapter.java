package com.laodev.tourlao.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.tourlao.R;

public class BannerAdapter extends BaseAdapter {

    private int[] images = {
            R.drawable.ic_etl,
            R.drawable.ic_plaza,
            R.drawable.ic_telecom
    };

    private int[] titles = {
            R.string.banner_etl_title,
            R.string.banner_plaza_title,
            R.string.banner_telecom_title,
    };

    private int[] descriptions = {
            R.string.banner_etl_desc,
            R.string.banner_plaza_desc,
            R.string.banner_telecom_desc,
    };

    private Context mContext;

    public BannerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_banner, null);

        ImageView img_banner = convertView.findViewById(R.id.img_banner);
        img_banner.setImageDrawable(mContext.getDrawable(images[position]));

        TextView lbl_title = convertView.findViewById(R.id.lbl_banner_title);
        lbl_title.setText(mContext.getString(titles[position]));

        TextView lbl_desc = convertView.findViewById(R.id.lbl_banner_desc);
        lbl_desc.setText(mContext.getString(descriptions[position]));

        return convertView;
    }

}
