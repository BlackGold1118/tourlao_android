package com.laodev.tourlao.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.tourlao.R;
import com.laodev.tourlao.model.CategoryModel;
import com.laodev.tourlao.model.ProvinceModel;
import com.laodev.tourlao.utils.AppUtils;

public class DialogProvinceAdapter extends BaseAdapter {

    private Context mContext;
    private DialogProvinceCallback dialogCategoryCallback;

    public DialogProvinceAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return AppUtils.gCategories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.item_dialog_province, null);

        ProvinceModel province = AppUtils.gProvinces.get(i);

        TextView lbl_name = view.findViewById(R.id.lbl_item_province_name);
        lbl_name.setText(province.name);

        TextView lbl_desc = view.findViewById(R.id.lbl_item_province_desc);
        lbl_desc.setText(province.desc);

        view.setOnClickListener(view1 -> dialogCategoryCallback.onSelectedProvinceList(province));

        return view;
    }

    public void setDialogProvinceCallback(DialogProvinceCallback dialogCategoryCallback) {
        this.dialogCategoryCallback = dialogCategoryCallback;
    }

    public interface DialogProvinceCallback {
        void onSelectedProvinceList(ProvinceModel model);
    }
}
