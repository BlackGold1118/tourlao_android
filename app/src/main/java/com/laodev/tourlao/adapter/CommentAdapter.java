package com.laodev.tourlao.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.tourlao.R;
import com.laodev.tourlao.model.CommentModel;
import com.laodev.tourlao.utils.AppUtils;
import com.laodev.tourlao.utils.FireManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends BaseAdapter {

    private Context mContext;
    private List<CommentModel> mComments;

    public CommentAdapter(Context contenxt, List<CommentModel> models) {
        mContext = contenxt;
        mComments = models;
    }

    @Override
    public int getCount() {
        return mComments.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.item_comment, null);

        CommentModel comment = mComments.get(i);

        ImageView img_avatar = view.findViewById(R.id.img_comment_avatar);

        ImageView img_star1 = view.findViewById(R.id.img_comment_star1);
        ImageView img_star2 = view.findViewById(R.id.img_comment_star2);
        ImageView img_star3 = view.findViewById(R.id.img_comment_star3);
        ImageView img_star4 = view.findViewById(R.id.img_comment_star4);
        ImageView img_star5 = view.findViewById(R.id.img_comment_star5);

        List<ImageView> images = new ArrayList<>();
        images.add(img_star1);
        images.add(img_star2);
        images.add(img_star3);
        images.add(img_star4);
        images.add(img_star5);

        for (int x = 0; x < 5; x++) {
            ImageView image = images.get(x);
            if (x < comment.rate) {
                image.setImageDrawable(mContext.getDrawable(R.drawable.ic_star_blue));
            } else {
                image.setImageDrawable(mContext.getDrawable(R.drawable.ic_star_border_blue));
            }
        }

        TextView lbl_name = view.findViewById(R.id.lbl_comment_name);
        TextView lbl_country = view.findViewById(R.id.lbl_comment_country);
        TextView lbl_date = view.findViewById(R.id.lbl_comment_date);
        lbl_date.setText(comment.date);
        TextView lbl_content = view.findViewById(R.id.lbl_comment_content);
        lbl_content.setText(comment.content);

        if (AppUtils.isFBMode) {
            FireManager.getUserFromFirebase(comment.userid, user -> {
                Picasso.with(mContext).load(user.photo).fit().centerCrop()
                        .placeholder(R.drawable.ic_user_avatar)
                        .error(R.drawable.ic_user_avatar)
                        .into(img_avatar, null);
                lbl_name.setText(user.name);
                lbl_country.setText(user.country);
            });
        } else {
            Picasso.with(mContext).load(comment.usr_img).fit().centerCrop()
                    .placeholder(R.drawable.ic_user_avatar)
                    .error(R.drawable.ic_user_avatar)
                    .into(img_avatar, null);
            lbl_name.setText(comment.usr_name);
            lbl_country.setText(comment.usr_country);
        }

        return view;
    }
}
