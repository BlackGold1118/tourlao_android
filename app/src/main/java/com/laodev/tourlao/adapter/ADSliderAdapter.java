package com.laodev.tourlao.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.tourlao.R;
import com.laodev.tourlao.model.ADModel;
import com.laodev.tourlao.utils.AppUtils;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ADSliderAdapter extends SliderViewAdapter<ADSliderAdapter.SliderAdapterVH> {

    private Context context;
    private List<ADModel> mModels;


    public ADSliderAdapter(Context context, List<ADModel> models) {
        this.context = context;
        mModels = models;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ad_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        ADModel model = mModels.get(position);

        viewHolder.textViewDescription.setText(model.name);
        Picasso.with(context).load(model.photo).fit().centerCrop()
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(viewHolder.imageViewBackground, null);
        viewHolder.itemView.setOnClickListener(view -> {
            String url = model.link;
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mModels.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.img_ad_slider);
            textViewDescription = itemView.findViewById(R.id.txt_ad_slider);
            this.itemView = itemView;
        }
    }

}
