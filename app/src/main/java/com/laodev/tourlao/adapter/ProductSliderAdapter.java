package com.laodev.tourlao.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.laodev.tourlao.R;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductSliderAdapter extends SliderViewAdapter<ProductSliderAdapter.SliderAdapterVH> {

    private Context context;
    private List<String> urls;

    public ProductSliderAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls = urls;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_detail_slider, null);
        return new ProductSliderAdapter.SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        String url = urls.get(position);
        Picasso.with(context).load(url).fit().centerCrop()
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(viewHolder.imageViewBackground, null);
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.img_detail_slider);
            this.itemView = itemView;
        }
    }

}
