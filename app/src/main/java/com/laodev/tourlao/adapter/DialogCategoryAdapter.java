package com.laodev.tourlao.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.laodev.tourlao.R;
import com.laodev.tourlao.model.CategoryModel;
import com.laodev.tourlao.utils.AppUtils;
import com.squareup.picasso.Picasso;

public class DialogCategoryAdapter extends BaseAdapter {

    private Context mContext;
    private DialogCategoryCallback dialogCategoryCallback;

    public DialogCategoryAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return AppUtils.gCategories.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.item_dialog_category, null);

        CategoryModel category = AppUtils.gCategories.get(i);

        ImageView img_category = view.findViewById(R.id.img_item_cateogry);
        Picasso.with(mContext).load(category.imgurl).fit().centerCrop()
                .placeholder(R.drawable.ic_image_blue)
                .error(R.drawable.ic_image_blue)
                .into(img_category, null);

        TextView lbl_category = view.findViewById(R.id.lbl_item_category);
        lbl_category.setText(category.name);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCategoryCallback.onSelectedCategoryList(category);
            }
        });

        return view;
    }

    public void setDialogCategoryCallback(DialogCategoryCallback dialogCategoryCallback) {
        this.dialogCategoryCallback = dialogCategoryCallback;
    }

    public interface DialogCategoryCallback {
        void onSelectedCategoryList(CategoryModel model);
    }
}
