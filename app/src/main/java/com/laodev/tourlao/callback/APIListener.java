package com.laodev.tourlao.callback;

import org.json.JSONObject;

/**
 * Created by asus on 2016/8/28.
 */

public interface APIListener {
    void onEventCallBack(JSONObject obj, int ret);
}
